import { createRouter, createWebHistory } from 'vue-router'
import {useAuthStore} from '../stores/auth' 
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/Login.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('../views/Register.vue')
    },
    {
      path: '/libros',
      name: 'libros',
      component: () => import('../views/Libros/index.vue')
    },
    {
      path: '/edit/:id',
      name: 'edit',
      component: () => import('../views/Libros/Edit.vue')
    },
     {
      path: '/autores/',
      name: 'autores',
      component: () => import('../views/Autores/index.vue')
    },
    {
      path: '/editar/:id',
      name: 'editar',
      component: () => import('../views/Autores/Edit.vue')
    },
    {
      path: '/crear/',
      name: 'crear',
      component: () => import('../views/Autores/Create.vue')
    },
    {
      path: '/usuarios/',
      name: 'usuarios',
      component: () => import('../views/Usuarios/inde.vue')
    },
    {
    path: '/edita/:id',
    name: 'edita',
    component: () => import('../views/Usuarios/Edit.vue')
  },
  {
    path: '/crea/',
    name: 'crea',
    component: () => import('../views/Usuarios/Create.vue')
  },
    {
      path: '/create',
      name: 'create',
      component: () => import('../views/Libros/Create.vue')
    },
    {
      path: '/prestamos',
      name: 'prestamos',
      component: () => import('../views/Prestamos/index.vue')
    },
    {
      path: '/graphic',
      name: 'graphic',
      component: () => import('../views/Prestamos/Graphic.vue')
    },
    {
      path: '/reports',
      name: 'reports',
      component: () => import('../views/Prestamos/Reports.vue')
    }
  ]
})

router.beforeEach( async (to) =>{
  const publicPages = ['/login','/register']
  const authRequired = ! publicPages.includes(to.path)
  const auth = useAuthStore()
  if(authRequired && !auth.user){
    auth.returnUrl=to.fullPath
    return '/login'

  }
})


export default router
